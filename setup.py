#!/usr/bin/env python

from distutils.core import setup

setup(
    name='parseapy',
    version='0.2',
    packages=['parseapy'],
    url='',
    license='Proprietery',
    author='Joost van Doremalen, David A. van Leeuwen',
    author_email='joost@novo-learning.com, david@novo-learning.com',
    description='Wrapper around Parse REST API'
)
