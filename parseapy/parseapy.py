#!/usr/bin/env python

from __future__ import print_function
from future import standard_library
standard_library.install_aliases()
from builtins import range
from builtins import object
__author__ = 'joostvandoremalen'

import json
import requests
import urllib.request, urllib.parse, urllib.error
import mimetypes
import logging

class Client(object):
    
    def __init__(self, app_id, master_key, base_url):
        self.app_id = app_id
        self.master_key = master_key
        self.base_url = base_url
        self.headers = {
            "X-Parse-Application-Id": self.app_id,
            "X-Parse-Master-Key": self.master_key,
            "Content-Type": "application/json"            
        }

    def encode_params(self, skip=None, limit=None, include=None, where=None):
        params = {}
        if limit:
            params['limit'] = limit
        if skip:
            params['skip'] = skip
        if include:
            params['include'] = include
        if where:
            params['where'] = json.dumps(where)
        return urllib.parse.urlencode(params)

    def get_user_from_session_token(self, session_token):
        headers = {
            "X-Parse-Application-Id": self.app_id,
            "X-Parse-Session-Token": session_token,
            "Content-Type": "application/json"
        }
        r = requests.get(self.base_url + '/users/me', headers=headers)
        r.raise_for_status()
        return r.json()

    def run_cloud_function(self, method, data):
        r = requests.post(self.base_url + '/functions/%s' % (method), data=json.dumps(data), headers=self.headers)
        ## logging.info("%d %r", r.status_code, r.text)
        result = r.json()
        return result

    def login(self, username, password):
        """Login using username/password, not relying on master key anymore"""
        self.headers["X-Parse-Master-Key"] = ""
        res = self.run_cloud_function("login", { "username": username, "password": password})
        if "result" in res and "user" in res["result"] and "sessionToken" in res["result"]["user"]:
            self.headers["X-Parse-Session-Token"] = res["result"]["user"]["sessionToken"]
        else:
            self.headers["X-Parse-Master-Key"] = self.master_key
            logging.warn("Could not log in, reverting to master key")

    def set_password(self, username, password):
        """Set a new password without knowing the old one, needs the master key"""
        self.headers["X-Parse-Master-Key"] = self.master_key
        res = self.run_cloud_function("setPasswordDirectly", { "username": username, "password": password})
        return res

    
    def run_cloud_job(self, method, data):
        r = requests.post(self.base_url + '/jobs/%s' % (method), data=json.dumps(data), headers=self.headers)
        result = r.json()
        return result
    
    def upload_file(self, fname, remote_fname):
        print('Upload file', fname)
        filetype = mimetypes.guess_type(fname)[0]
        print(filetype)
    
        file_upload_headers = {
            "X-Parse-Application-Id": self.app_id,
            "X-Parse-Master-Key":  self.master_key,
            "Content-Type": filetype
        }
    
        assert filetype, "Filetype for %s could not be established." % fname
        data =  open(fname, 'rb').read()
        r = requests.post(self.base_url + '/files/'+ remote_fname, data=data, headers=file_upload_headers)
        result = r.json()
        print(result)
        return result
    
    def pointer(self, class_name, object_id):
        return {
            '__type': 'Pointer',
            'className': class_name,
            'objectId': object_id
        }

    # Objects
    
    def get_object(self, class_name, object_id, include=None, where=None):
        params = self.encode_params(include=include, where=where)
        r = requests.get(self.base_url + '/classes/%s/%s?%s' % (class_name, object_id, params), headers=self.headers)
        return r.json()

    def update_object(self, class_name, object_id, data):
        r = requests.put(self.base_url + '/classes/%s/%s' % (class_name, object_id), data=json.dumps(data), headers=self.headers)
        result = r.json()
        return result
    
    def create_object(self, class_name, data):
        r = requests.post(self.base_url + '/classes/%s' % (class_name), data=json.dumps(data), headers=self.headers)
        result = r.json()
        return result

    def delete_object(self, class_name, object_id):
        r = requests.delete(self.base_url + '/classes/%s/%s' % (class_name, object_id), headers=self.headers)
        return r.json()

    def get_objects(self, class_name, include=None, where=None):
        limit = 1000
        skip = 0
        all = []
        path = '/users' if class_name == 'User' else '/classes/%s' % class_name
        while True:
            params = self.encode_params(skip, limit, include, where)
            r = requests.get(self.base_url + '%s?%s' % (path, params), headers=self.headers)
            result = r.json()
            assert 'results' in result, result
            objects = result['results']
            if len(objects) == 0:
                break
            else:
                all.extend(objects)
                skip += limit
        return all

    # Roles
    
    def create_role(self, data):
        r = requests.post(self.base_url + '/roles', data=json.dumps(data), headers=self.headers)
        result = r.json()
        return result
    
    def update_role(self, object_id, data):
        r = requests.put(self.base_url + '/roles/%s' % (object_id), data=json.dumps(data), headers=self.headers)
        result = r.json()
        return result
    
    def get_role(self, object_id, include=None, where=None):
        params = self.encode_params(include=include, where=where)
        r = requests.get(self.base_url + '/roles/%s?%s' % (object_id, params), headers=self.headers)
        return r.json()

    def get_roles(self, include=None, where=None):
        limit = 1000
        skip = 0
        all = []
        while True:
            params = self.encode_params(skip, limit, include, where)
            r = requests.get(self.base_url + '/roles?%s' % (params), headers=self.headers)
            result = r.json()
            assert 'results' in result, result
            objects = result['results']
            if len(objects) == 0:
                break
            else:
                all.extend(objects)
                skip += limit
        return all
    

   # Relations
    
    def add_objects_to_relation(self, class_name, relation_id, objects):
        r = requests.put(self.base_url + '/classes/%s/%s' % (class_name, relation_id), data=json.dumps({
            'users': {
                 '__op': "AddRelation",
                 'objects': objects
            }
        }), headers=self.headers)
        result = r.json()
        return result


    # Schemas
    
    def get_schemas(self):
        r = requests.get(self.base_url + '/schemas', headers=self.headers)
        return r.json()
    
    def create_schema(self, class_name, schema):
        r = requests.post(self.base_url + '/schemas/%s' % class_name, data=json.dumps(schema), headers=self.headers)
        return r.json()
    
    def delete_schema(self, class_name, schema):
        r = requests.delete(self.base_url + '/schemas/%s' % class_name, headers=self.headers)
        return r.json()


    # Config
    
    def get_config(self):
        r = requests.get(self.base_url + '/config', headers=self.headers)
        return r.json()


    # Batch
    
    def batch(self, reqs):
        nr_reqs_in_batch = 50
        for i in range(0, len(reqs), nr_reqs_in_batch):
            reqs_in_batch = reqs[i:i+nr_reqs_in_batch]
            r = requests.post(self.base_url + '/batch', data=json.dumps({
               "requests": reqs_in_batch
            }), headers=self.headers)
            result = r.json()
            assert 'results' in result, result