#!/usr/bin/env python

from builtins import object
import base64
import requests
import json

class LRSClient(object):
    def __init__(self, url, username, password):
        self.url = url
        self.endpoint = url + '/api/statements/aggregate'
        basicAuth = base64.b64encode(username + ':' + password)
        self.headers = {
            "Authorization": "Basic " + basicAuth,
            "X-Experience-API-Version": '1.0.0'
        }

    def sendRequest(self, query):
        pipeline = query + [
            { "$project": { 'statement': 1, 'timestamp': 1 } },
            { "$sort": { "timestamp": 1 } }
        ]
        params = {
            "pipeline": json.dumps(query),
            "cache": False
        }
        res = requests.get(self.endpoint, headers=self.headers, params=params)
        return res.json()
