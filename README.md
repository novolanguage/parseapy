# Python wrapper for Parse REST API

Example usage:
```python
import parseapy

client = parseapy.Client(app_id, master_key, base_url)
lessons = client.get_objects('Lesson')
```

## Logging in with credentials

Instead of using the master key, you can now log in using an email/password combo:
```python
c = parseapy.Client("novo", "", "https://platform-test.novo-learning.com/parse"
c.login(email, password)

c.run_cloud_function("getUserPermissions", {})
